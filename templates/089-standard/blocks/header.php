<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header id="header" class="fullwidth <?php print(!$detect->isMobile()) ? "largeBckgr" : (($detect->isTablet()) ? "mediumBckgr" : "smallBckgr"); ?>">
	<?php /*if(!$detect->isMobile() || $detect->isTablet()) : ?>
		<img id="headerIMG" src="/images/header-bckgr.jpg" srcset="/images/header-bckgr-large.jpg 1x, /images/header-bckgr-medium.jpg 2x" alt="Estrich verlegen Meisterbetrieb München Dennis Pekesin Hintergrundbild"/>
	<?php endif; */?>	
	<div class="innerwidth logoVcard-Wrapper">
	<div class="row-fluid">        			
		<div class="span6">
			<div class="logoWrapper <?php print ($detect->isMobile() ) ? "mobile" : "desktop"; ?>">
				<a class="brand" href="<?php echo $this->baseurl; ?>">
				<?php echo $logo; ?>
					<?php if ($this->params->get('sitedescription')) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
					<?php endif; ?>
				</a>
			</div>
		</div>
		<div class="span6">
			<div class="vcard clr">				  
			  <p class="tel "><span>Service-Telefon</span><br /><a class="" href="tel:+4982028591">08202 - 8591</a></p>
			  <p class="adr"><span class="street-address">Aumoosstr. 5</span><br />
			    <span class="postal-code">82293 </span><span class="region">Vogach</span>
			  </p>
			</div>
		</div>	          
	</div>
	</div>
<?php if(!$detect->isMobile() || $detect->isTablet() ) : ?>
	<div class="sloganHeader clr">
    	<h3>Darauf werden Sie stehen!</h3>
	</div>
<?php endif; ?> 
</header>