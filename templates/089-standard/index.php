<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang ="de-De">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class
	. $detectAgent; print ($clientMobile) ? "mobile " : " ";
?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php			
			
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

			// including breadcrumb
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
								
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
			
			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript">

		jQuery(document).ready(function() {
			<?php if ($detect->isMobile()) : ?> //mobile

					jQuery.extend(jQuery.lazyLoadXT, { // lazyLoad
						  edgeY:  50,
						  srcAttr: 'data-src'
						});		
					<?php else : ?> //desktop
						jQuery.extend(jQuery.lazyLoadXT, {
							  edgeY:  50,
						  	srcAttr: 'data-src'
						});    

			<?php endif;  ?>

		 jQuery(window).scroll(function(){ // sticky top Menue
		 	//console.log(jQuery(document).scrollTop());
			if (jQuery(window).scrollTop() > 150 ) {
					jQuery('.estLiveOverlay, .liveBanner').css("opacity", "1");

			} 
		});
	});

	</script>

</body>
</html>
